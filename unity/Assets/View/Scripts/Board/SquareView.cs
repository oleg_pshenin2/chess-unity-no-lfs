﻿using UnityEngine;

namespace TestChess.View
{
    public class SquareView : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        public void SetAsHovered(bool hovered)
        {
            _animator.SetBool("Hovered", hovered);
        }

        public void SetAsSelected(bool selected)
        {
            _animator.SetBool("Selected", selected);
        }

        public void SetAsAvailableToMove(bool availableToMove)
        {
            _animator.SetBool("PossibleMove", availableToMove);
        }

        public void SetAsAvailableToCapture(bool availableToCapture)
        {
            _animator.SetBool("AvailableToCapture", availableToCapture);
        }
    }
}