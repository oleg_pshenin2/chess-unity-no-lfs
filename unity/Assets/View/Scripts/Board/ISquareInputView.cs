using System;

namespace TestChess.View
{
    public interface ISquareInputView
    {
        event Action<int> PointerEnter;
        event Action<int> PointerExit;
        event Action<int> PointerUp;
        event Action<int> PointerDown;
        event Action<ISquareInputView> Destroyed;

        void Init(int index);
    }
}