using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TestChess.View
{
    public class GameResultView : MonoBehaviour
    {
        public event Action RestartButtonClicked = delegate { };

        [SerializeField] private GameObject _winHolder;
        [SerializeField] private GameObject _drawHolder;
        [SerializeField] private GameObject _patHolder;

        [SerializeField] private TextMeshProUGUI _winText;
        [SerializeField] private TextMeshProUGUI _drawText;
        [SerializeField] private TextMeshProUGUI _patText;

        [SerializeField] private GameObject _restartButtonHolder;
        [SerializeField] private Button _restartButton;

        [SerializeField] private string _whitePlayerName;
        [SerializeField] private string _blackPlayerName;
        [SerializeField] private string _winStringMask = "{0} won!";
        [SerializeField] private string _drawStringMask = "Draw by {0}!";
        [SerializeField] private string _patStringMask = "Pat by {0}!";


        private void Awake()
        {
            _restartButton.onClick.RemoveAllListeners();
            _restartButton.onClick.AddListener(OnRestartButtonClicked);
        }

        public void Win(bool isWhite)
        {
            _winText.text = string.Format(_winStringMask, GetPlayerName(isWhite));
            _winHolder.SetActive(true);

            _restartButtonHolder.SetActive(true);
        }

        public void Draw(bool isWhite)
        {
            _drawText.text = string.Format(_drawStringMask, GetPlayerName(isWhite));
            _drawHolder.SetActive(true);

            _restartButtonHolder.SetActive(true);
        }

        public void Pat(bool isWhite)
        {
            _patText.text = string.Format(_patStringMask, GetPlayerName(isWhite));
            _patHolder.SetActive(true);

            _restartButtonHolder.SetActive(true);
        }

        public void HideAll()
        {
            _winHolder.SetActive(false);
            _drawHolder.SetActive(false);
            _patHolder.SetActive(false);

            _restartButtonHolder.SetActive(false);
        }

        private string GetPlayerName(bool isWhite)
        {
            return isWhite ? _whitePlayerName : _blackPlayerName;
        }

        private void OnRestartButtonClicked()
        {
            RestartButtonClicked();
            HideAll();
        }
    }
}