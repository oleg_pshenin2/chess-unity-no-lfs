using TMPro;
using UnityEngine;

namespace TestChess.View
{
    public class CurrentTurnNumberView : MonoBehaviour
    {
        [SerializeField] private string _turnNumberStringMask = "Current turn number {0}";
        [SerializeField] private TextMeshProUGUI _turnNumberText;

        public void SetTurnNumber(int turnNumber)
        {
            var playersUnitedTurn = (turnNumber - 1) / 2 + 1;
            _turnNumberText.text = string.Format(_turnNumberStringMask, playersUnitedTurn);
        }
    }
}