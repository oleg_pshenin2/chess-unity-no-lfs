using TMPro;
using UnityEngine;

namespace TestChess.View
{
    public class CurrentTurnSideView : MonoBehaviour
    {
        [SerializeField] private string _turnSideWhiteText = "Current turn: White";
        [SerializeField] private string _turnSideBlackText = "Current turn: Black";
        [SerializeField] private TextMeshProUGUI _turnNumberText;

        public void SetTurnWhite()
        {
            _turnNumberText.text = _turnSideWhiteText;
        }

        public void SetTurnBlack()
        {
            _turnNumberText.text = _turnSideBlackText;
        }
    }
}