using TestChess.Model;
using TestChess.View;
using UnityEngine;

namespace TestChess.Presenter
{
    public class CurrentTurnInfoPresenter : MonoBehaviour
    {
        [SerializeField] private CurrentTurnSideView _currentTurnSideView;
        [SerializeField] private CurrentTurnNumberView _currentTurnNumberView;
        [SerializeField] private CurrentTurnCheckView _currentTurnCheckView;
        private ChessGame _game;

        public void Init(ChessGame game)
        {
            Unsubscribe();

            _game = game;
            RefreshTurnInfo();

            Subscribe();
        }

        private void Subscribe()
        {
            _game.TurnFinished += RefreshTurnInfo;
        }

        private void Unsubscribe()
        {
            if (_game == null)
                return;

            _game.TurnFinished -= RefreshTurnInfo;
        }

        private void RefreshTurnInfo()
        {
            _currentTurnNumberView.SetTurnNumber(_game.CurrentTurnNumber);

            if (_game.CurrentTurnSide == ChessColor.White)
            {
                _currentTurnSideView.SetTurnWhite();
            }
            else
            {
                _currentTurnSideView.SetTurnBlack();
            }

            if (_game.IsCheckState)
            {
                _currentTurnCheckView.EnableCheckStatus();
            }
            else
            {
                _currentTurnCheckView.DisableCheckStatus();
            }
        }

        private void OnDestroy()
        {
            Unsubscribe();
        }
    }
}