using System;
using TestChess.Model;
using TestChess.View;
using UnityEngine;

namespace TestChess.Presenter
{
    public class GameResultPresenter : MonoBehaviour
    {
        [SerializeField] private GameResultView _gameResultView;
        private AppController _appController;
        private ChessGame _game;

        public void Init(AppController appController, ChessGame game)
        {
            Unsubscribe();

            _appController = appController;
            _game = game;

            Subscribe();
        }

        private void Subscribe()
        {
            _game.GameFinished += RefreshGameResultScreen;
            _gameResultView.RestartButtonClicked += TryToRestart;
        }

        private void Unsubscribe()
        {
            if (_game != null)
                _game.GameFinished -= RefreshGameResultScreen;

            if (_gameResultView != null)
                _gameResultView.RestartButtonClicked -= TryToRestart;
        }

        private void RefreshGameResultScreen(ChessGameEnd chessGameEnd, ChessColor side)
        {
            var isWhite = side == ChessColor.White;

            switch (chessGameEnd)
            {
                case ChessGameEnd.Win:
                    _gameResultView.Win(isWhite);
                    break;
                case ChessGameEnd.Draw:
                    _gameResultView.Draw(isWhite);
                    break;
                case ChessGameEnd.Pat:
                    _gameResultView.Pat(isWhite);
                    break;
                default:

                    break;
            }
        }

        private void TryToRestart()
        {
            _appController.RestartGame();
        }

        private void OnDestroy()
        {
            Unsubscribe();
        }
    }
}