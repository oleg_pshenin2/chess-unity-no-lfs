using System;
using TestChess.Model;

namespace TestChess.Presenter
{
    public class MovesHandler
    {
        public event Action<int> PieceSelected = delegate { };
        public event Action PieceDeselected = delegate { };

        private ChessGame _game;
        private SquaresInputPresenter _squaresInputHandler;
        private int _selectedPieceIndex;

        public MovesHandler(ChessGame game, SquaresInputPresenter squaresInputHandler)
        {
            Unsubscribe();

            _game = game;
            _squaresInputHandler = squaresInputHandler;

            ResetSelectedPiece();

            Subscribe();
        }

        private void Subscribe()
        {
            _squaresInputHandler.SquareClicked += SquareClickedHandler;
        }

        private void Unsubscribe()
        {
            if (_squaresInputHandler != null)
                _squaresInputHandler.SquareClicked -= SquareClickedHandler;
        }

        private void SquareClickedHandler(int index)
        {
            if (_selectedPieceIndex >= 0)
            {
                if (_selectedPieceIndex == index)
                    return;

                var pieceOnClickedSquare = _game.Board.GetPiece(index);

                if (pieceOnClickedSquare != null && pieceOnClickedSquare.Color == _game.CurrentTurnSide)
                {
                    SelectPiece(index);
                    return;
                }

                _game.MakeAMove(_selectedPieceIndex, index);

                ResetSelectedPiece();
                return;
            }
            else
            {
                var pieceOnClickedSquare = _game.Board.GetPiece(index);
                if (pieceOnClickedSquare == null)
                    return;

                if (pieceOnClickedSquare.Color != _game.CurrentTurnSide)
                    return;

                SelectPiece(index);
            }
        }

        private void SelectPiece(int index)
        {
            PieceSelected(index);
            _selectedPieceIndex = index;
        }

        private void ResetSelectedPiece()
        {
            PieceDeselected();
            _selectedPieceIndex = -1;
        }
    }
}