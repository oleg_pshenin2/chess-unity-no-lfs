using System;
using TestChess.Model;
using TestChess.View;
using UnityEngine;

namespace TestChess.Presenter
{
    public class SquaresInputPresenter : MonoBehaviour
    {
        public event Action<int> SquareHovered = delegate { };
        public event Action<int> SquareUnhovered = delegate { };
        public event Action<int> SquareClicked = delegate { };

        private IBoardLayoutProvider _boardLayoutProvider;
        private ChessGame _game;
        private int _lastPointerDown = -1;

        [SerializeField] private GameObject _squareInputPrefab;
        [SerializeField] private Transform _root;


        public void Init(IBoardLayoutProvider boardLayoutProvider, ChessGame game)
        {
            _boardLayoutProvider = boardLayoutProvider;
            _game = game;
            GenerateSquares();
        }

        private void GenerateSquares()
        {
            _root.DestroyAllChildren();
            BoardUtils.IterateThroughBoard(GenerateSquare);
        }

        private void GenerateSquare(int x, int y)
        {
            var go = Instantiate(_squareInputPrefab, _root);
            var index = BoardUtils.GetIndexByCoords(x, y);
            go.transform.position = _boardLayoutProvider.GetLayoutByIndex(index).position;

            var squareInput = go.GetComponent<ISquareInputView>();
            squareInput.Init(index);
            SubscribeTo(squareInput);
        }

        private void SubscribeTo(ISquareInputView squareInput)
        {
            squareInput.PointerEnter += HandlePointerEnter;
            squareInput.PointerExit += HandlePointerExit;
            squareInput.PointerUp += HandlePointerUp;
            squareInput.PointerDown += HandlePointerDown;

            squareInput.Destroyed += UnsubscribeFrom;
        }

        private void UnsubscribeFrom(ISquareInputView squareInput)
        {
            if (squareInput == null)
                return;

            squareInput.PointerEnter -= HandlePointerEnter;
            squareInput.PointerExit -= HandlePointerExit;
            squareInput.PointerUp -= HandlePointerUp;
            squareInput.PointerDown -= HandlePointerDown;
        }

        private void HandlePointerEnter(int index)
        {
            SquareHovered(index);
        }

        private void HandlePointerExit(int index)
        {
            SquareUnhovered(index);
        }

        private void HandlePointerDown(int index)
        {
            _lastPointerDown = index;
        }

        private void HandlePointerUp(int index)
        {
            if (index == _lastPointerDown)
            {
                OnSquareClicked(index);
            }
        }

        private void OnSquareClicked(int index)
        {
            SquareClicked(index);
        }
    }
}