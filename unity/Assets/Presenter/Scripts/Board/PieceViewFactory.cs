﻿using PB;
using TestChess.Model;
using TestChess.View;
using UnityEngine;

namespace TestChess.Presenter
{
    public class PieceViewFactory : MonoBehaviour
    {
        [SerializeField] private GameObject _piecePrefab;
        [SerializeField] private ScriptableSpritesSet _blackPieces;
        [SerializeField] private ScriptableSpritesSet _whitePieces;

        public PieceView GetPiece(PieceType pieceType, ChessColor color)
        {
            var peiceGO = Instantiate(_piecePrefab);
            var pieceUid = pieceType.ToString();

            var isWhite = (color == ChessColor.White);

            var sprite = isWhite ? _whitePieces.GetSpriteByUid(pieceUid) : _blackPieces.GetSpriteByUid(pieceUid);
            var view = peiceGO.GetComponent<PieceView>();
            view.SetupIcon(sprite);

            return view;
        }
    }
}