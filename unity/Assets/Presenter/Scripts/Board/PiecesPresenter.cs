using System.Collections.Generic;
using DG.Tweening;
using TestChess.Model;
using TestChess.View;
using UnityEngine;

namespace TestChess.Presenter
{
    public class PiecesPresenter : MonoBehaviour
    {
        [SerializeField] private Transform _root;
        [SerializeField] private PieceViewFactory _pieceViewFactory;
        private Dictionary<int, PieceView> _piecesByIndex = new Dictionary<int, PieceView>();
        private IBoardLayoutProvider _boardLayoutProvider;
        private ChessGame _game;

        public void Init(IBoardLayoutProvider boardLayoutProvider, ChessGame game)
        {
            Unsubscribe();

            _boardLayoutProvider = boardLayoutProvider;
            _game = game;

            InitBoard();
            Subscribe();
        }

        private void Subscribe()
        {
            _game.Board.PieceMovedFromTo += MovePieceFromTo;
            _game.PiecePromotedAt += RefreshPieceAt;
        }

        private void Unsubscribe()
        {
            if (_game != null)
            {
                if (_game.Board != null)
                    _game.Board.PieceMovedFromTo -= MovePieceFromTo;

                _game.PiecePromotedAt -= RefreshPieceAt;
            }
        }

        private void InitBoard()
        {
            _piecesByIndex.Clear();
            _root.DestroyAllChildren();

            var squares = _game.Board.GetSquares();

            foreach (var square in squares)
            {
                if (square.Piece != null)
                {
                    CreatePieceAt(square.Index, square.Piece);
                }
            }
        }

        private void CreatePieceAt(int index, Piece piece)
        {
            RemovePieceAt(index);

            var pieceView = _pieceViewFactory.GetPiece(piece.GetPieceType(), piece.Color);

            pieceView.transform.SetParent(_root);
            pieceView.transform.position = _boardLayoutProvider.GetLayoutByIndex(index).position;

            _piecesByIndex[index] = pieceView;
        }

        private void RemovePieceAt(int index)
        {
            if (_piecesByIndex.ContainsKey(index))
            {
                if (_piecesByIndex[index] != null)
                    _piecesByIndex[index].Destroy();

                _piecesByIndex.Remove(index);
            }
        }

        private void CapturePieceAt(int index)
        {
            if (_piecesByIndex.ContainsKey(index))
            {
                if (_piecesByIndex[index] != null)
                    _piecesByIndex[index].Capture();

                _piecesByIndex.Remove(index);
            }
        }

        private void MovePieceFromTo(int fromIndex, int toIndex)
        {
            CapturePieceAt(toIndex);

            var pieceView = _piecesByIndex[fromIndex];

            pieceView.transform.SetAsLastSibling();
            pieceView.MoveTo(_boardLayoutProvider.GetLayoutByIndex(toIndex).position);

            _piecesByIndex[toIndex] = pieceView;
            _piecesByIndex.Remove(fromIndex);
        }

        private void RefreshPieceAt(int promotionIndex)
        {
            RemovePieceAt(promotionIndex);

            var newPiece = _game.Board.GetPiece(promotionIndex);
            CreatePieceAt(promotionIndex, newPiece);
        }

        private void OnDestroy()
        {
            Unsubscribe();
        }
    }
}