using UnityEngine;

namespace TestChess.Presenter
{
    public interface IBoardLayoutProvider
    {
        Transform GetLayoutByIndex(int index);
    }
}