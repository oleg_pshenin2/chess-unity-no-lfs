using System;
using System.Collections.Generic;
using System.Linq;
using TestChess.Model;
using UnityEngine;
using UnityEngine.UI;

namespace TestChess.Presenter
{
    public class BoardLayoutPresenter : MonoBehaviour, IBoardLayoutProvider
    {
        [Serializable]
        private class IndexLayoutPair
        {
            public int Index;
            public Transform Layout;
        }

        [SerializeField] private Transform _root;
        [SerializeField] private GameObject _layoutPrefab;
        [SerializeField] private GridLayoutGroup _gridLayoutGroup;
        [SerializeField] private List<IndexLayoutPair> _indexLayoutPairsList;
        private Dictionary<int, Transform> _layoutByIndexDictionary;
        private bool _mapped;

        [ContextMenu("Generate Layout")]
        private void GenerateLayout()
        {
            _indexLayoutPairsList = new List<IndexLayoutPair>();
            _mapped = false;
            foreach (Transform child in _root)
            {
                DestroyImmediate(child.gameObject);
            }

            _gridLayoutGroup.enabled = true;

            BoardUtils.IterateThroughBoard(GenerateLayout);

            Canvas.ForceUpdateCanvases();
            _gridLayoutGroup.enabled = false;
        }

        private void GenerateLayout(int x, int y)
        {
            var index = BoardUtils.GetIndexByCoords(x, y);
            var go = Instantiate(_layoutPrefab, _root);
            go.name = $"{x} : {y} - {index}";

            _indexLayoutPairsList.Add(new IndexLayoutPair()
            {
                Index = index,
                Layout = go.transform,
            });
        }

        private void TryToMapDictionary()
        {
            if (!_mapped)
            {
                _layoutByIndexDictionary = _indexLayoutPairsList.ToDictionary(x => x.Index, x => x.Layout);
                _mapped = true;
            }
        }

        public Transform GetLayoutByIndex(int index)
        {
            TryToMapDictionary();

            if (_layoutByIndexDictionary == null || !_layoutByIndexDictionary.ContainsKey(index))
            {
                Debug.LogError($"Couldn't find layout for square with index {index}, please check that layout layer was generated correctly!");
                return null;
            }
            else
            {
                return _layoutByIndexDictionary[index];
            }
        }
    }
}