using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PB
{
    // Need to inherit your own pair because unity doesn't support serialization of generics
    [Serializable]
    public class Pair<K, V>
    {
        public K Key;
        public V Value;
    }

    [Serializable]
    public class String_GameObject_Pair : Pair<string, GameObject> { }

    [Serializable]
    public class String_Sprite_Pair : Pair<string, Sprite> { }

    [Serializable]
    public class String_TextAsset_Pair : Pair<string, TextAsset> { }

    public class ScriptableGenericSet<K, V, TPair> : ScriptableObject where TPair : Pair<K, V> where V : class
    {
        [SerializeField] protected List<TPair> _pairs;
        private Dictionary<K, V> _valueByKey = new Dictionary<K, V>();

        private void InitIfNeeded()
        {
            if (_valueByKey == null || _valueByKey.Count == 0)
                _valueByKey = _pairs.ToDictionary(x => x.Key, x => x.Value);
        }

        public List<K> GetListOfKeys()
        {
            InitIfNeeded();

            return _valueByKey.Keys.ToList();
        }

        public void Clear()
        {
            _pairs.Clear();
        }

        protected V GetValueByUid(K uid)
        {
            InitIfNeeded();

            if (_valueByKey.ContainsKey(uid))
            {
                return _valueByKey[uid];
            }
            else
            {
                Debug.LogError($"Incorrect uid: {uid.ToString()}");
                return null;
            }
        }
    }
}