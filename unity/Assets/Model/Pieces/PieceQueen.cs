namespace TestChess.Model
{
    public sealed class PieceQueen : Piece
    {
        private readonly PieceMovement[] _pieceMovementSet = new PieceMovement[8]
        {
            new PieceMovementDirectional (PieceMovementDirections.Up),
            new PieceMovementDirectional (PieceMovementDirections.UpRight),
            new PieceMovementDirectional (PieceMovementDirections.Right),
            new PieceMovementDirectional (PieceMovementDirections.DownRight),
            new PieceMovementDirectional (PieceMovementDirections.Down),
            new PieceMovementDirectional (PieceMovementDirections.DownLeft),
            new PieceMovementDirectional (PieceMovementDirections.Left),
            new PieceMovementDirectional (PieceMovementDirections.UpLeft),
        };

        public PieceQueen(ChessColor color) : base(color) { }

        public override PieceMovement[] GetPieceMovementSet()
        {
            return _pieceMovementSet;
        }

        public override PieceType GetPieceType()
        {
            return PieceType.Queen;
        }
    }
}