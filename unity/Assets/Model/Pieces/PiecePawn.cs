namespace TestChess.Model
{
    public sealed class PiecePawn : Piece
    {
        private const PieceMovementDirections WHITE_MOVE_DIRECTION = PieceMovementDirections.Up;
        private const PieceMovementDirections BLACK_MOVE_DIRECTION = PieceMovementDirections.Down;
        private const int UNMOVED_LENGTH = 2;
        private const int MOVED_LENGTH = 1;

        private readonly PieceMovement[] _pieceMovementSetUnmovedWhite = new PieceMovement[1]
        {
            new PieceMovementDirectionalLimited (WHITE_MOVE_DIRECTION, UNMOVED_LENGTH),
        };

        private readonly PieceMovement[] _pieceMovementSetMovedWhite = new PieceMovement[1]
        {
            new PieceMovementDirectionalLimited (WHITE_MOVE_DIRECTION, MOVED_LENGTH),
        };

        private readonly PieceMovement[] _pieceMovementSetUnmovedBlack = new PieceMovement[1]
        {
            new PieceMovementDirectionalLimited (BLACK_MOVE_DIRECTION, UNMOVED_LENGTH),
        };

        private readonly PieceMovement[] _pieceMovementSetMovedBlack = new PieceMovement[1]
        {
            new PieceMovementDirectionalLimited (BLACK_MOVE_DIRECTION, MOVED_LENGTH),
        };

        private readonly PieceMovement[] _pieceAttackSetWhite = new PieceMovement[2]
        {
            new PieceMovementDirectionalLimited (PieceMovementDirections.UpLeft, 1),
            new PieceMovementDirectionalLimited (PieceMovementDirections.UpRight, 1),
        };

        private readonly PieceMovement[] _pieceAttackSetBlack = new PieceMovement[2]
        {
            new PieceMovementDirectionalLimited (PieceMovementDirections.DownLeft, 1),
            new PieceMovementDirectionalLimited (PieceMovementDirections.DownRight, 1),
        };

        public PiecePawn(ChessColor color) : base(color) { }

        public override PieceMovement[] GetPieceMovementSet()
        {
            if (IsMoved())
            {
                if (Color == ChessColor.White)
                    return _pieceMovementSetMovedWhite;
                else
                    return _pieceMovementSetMovedBlack;
            }
            else
            {
                if (Color == ChessColor.White)
                    return _pieceMovementSetUnmovedWhite;
                else
                    return _pieceMovementSetUnmovedBlack;
            }
        }

        public override PieceType GetPieceType()
        {
            return PieceType.Pawn;
        }

        public override bool IsAttackSetTheSameAsMovement()
        {
            return false;
        }

        public override PieceMovement[] GetPieceAttackSet()
        {
            if (Color == ChessColor.White)
                return _pieceAttackSetWhite;
            else
                return _pieceAttackSetBlack;
        }

        public override bool CanBePromoted()
        {
            return true;
        }
    }
}