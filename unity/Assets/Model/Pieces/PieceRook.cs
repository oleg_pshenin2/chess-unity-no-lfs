namespace TestChess.Model
{
    public sealed class PieceRook : Piece
    {
        private readonly PieceMovement[] _pieceMovementSet = new PieceMovement[4]
        {
            new PieceMovementDirectional (PieceMovementDirections.Up),
            new PieceMovementDirectional (PieceMovementDirections.Right),
            new PieceMovementDirectional (PieceMovementDirections.Down),
            new PieceMovementDirectional (PieceMovementDirections.Left),
        };

        public PieceRook(ChessColor color) : base(color) { }

        public override PieceMovement[] GetPieceMovementSet()
        {
            return _pieceMovementSet;
        }

        public override PieceType GetPieceType()
        {
            return PieceType.Rook;
        }
    }
}