using System.Collections.Generic;

namespace TestChess.Model
{
    public class PieceMovementOffsetJumpLimited : PieceMovementOffsetJump
    {
        private readonly int _length;

        public PieceMovementOffsetJumpLimited(Coords jumpOffset, int length) : base(jumpOffset)
        {
            _length = length;
        }

        public override IEnumerator<Coords> GetEnumerator()
        {
            return new PieceMovementDirectionalEnumerator(JumpOffset, PieceCoords, _length);
        }
    }
}
