using System.Collections.Generic;

namespace TestChess.Model
{
    public partial class PieceMovementOffsetJump : PieceMovement
    {
        protected readonly Coords JumpOffset;
        protected Coords PieceCoords;

        public PieceMovementOffsetJump(Coords jumpOffset)
        {
            JumpOffset = jumpOffset;
        }

        public override void SetupPieceCoords(Coords pieceCoords)
        {
            PieceCoords = pieceCoords;
        }

        public override IEnumerator<Coords> GetEnumerator()
        {
            return new PieceMovementDirectionalEnumerator(JumpOffset, PieceCoords);
        }
    }
}