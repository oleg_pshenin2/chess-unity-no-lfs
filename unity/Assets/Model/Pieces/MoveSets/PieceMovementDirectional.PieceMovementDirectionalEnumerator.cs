using System.Collections;
using System.Collections.Generic;

namespace TestChess.Model
{
    public partial class PieceMovementDirectional
    {
        protected class PieceMovementDirectionalEnumerator : IEnumerator<Coords>
        {
            private static readonly Dictionary<PieceMovementDirections, Coords> _directionsOffsets = new Dictionary<PieceMovementDirections, Coords>()
            {
                {PieceMovementDirections.Up, new Coords(0, 1)},
                {PieceMovementDirections.UpRight, new Coords(1, 1)},
                {PieceMovementDirections.Right, new Coords(1, 0)},
                {PieceMovementDirections.DownRight, new Coords(1, -1)},
                {PieceMovementDirections.Down, new Coords(0, -1)},
                {PieceMovementDirections.DownLeft, new Coords(-1, -1)},
                {PieceMovementDirections.Left, new Coords(-1, 0)},
                {PieceMovementDirections.UpLeft, new Coords(-1, 1)},
            };

            private readonly PieceMovementDirections _direction;
            private readonly Coords _pieceCoords;
            private readonly int _movesLimit;
            private int _currentIterationNumber;

            public Coords Current { get; private set; }
            object IEnumerator.Current => this.Current;

            public PieceMovementDirectionalEnumerator(PieceMovementDirections direction, Coords pieceCoords, int movesLimit = 0)
            {
                _direction = direction;
                _pieceCoords = pieceCoords;
                _movesLimit = movesLimit;

                Current = pieceCoords;
            }

            public bool MoveNext()
            {
                if (IsLimited())
                {
                    _currentIterationNumber++;
                    if (_currentIterationNumber > _movesLimit)
                        return false;
                }

                if (_directionsOffsets.ContainsKey(_direction))
                    Current += _directionsOffsets[_direction];

                return true;
            }

            public void Reset()
            {
                Current = _pieceCoords;
                _currentIterationNumber = 0;
            }

            public void Dispose() { }

            private bool IsLimited()
            {
                return _movesLimit > 0;
            }
        }
    }
}