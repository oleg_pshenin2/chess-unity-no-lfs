using System.Collections.Generic;

namespace TestChess.Model
{
    public class PieceMovementDirectionalLimited : PieceMovementDirectional
    {
        private readonly int _length;

        public PieceMovementDirectionalLimited(PieceMovementDirections direction, int length) : base(direction)
        {
            _length = length;
        }

        public override IEnumerator<Coords> GetEnumerator()
        {
            return new PieceMovementDirectionalEnumerator(Direction, PieceCoords, _length);
        }
    }
}