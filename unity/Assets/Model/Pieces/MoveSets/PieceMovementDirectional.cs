using System.Collections.Generic;

namespace TestChess.Model
{
    public partial class PieceMovementDirectional : PieceMovement
    {
        protected readonly PieceMovementDirections Direction;
        protected Coords PieceCoords;

        public PieceMovementDirectional(PieceMovementDirections direction)
        {
            Direction = direction;
        }

        public override void SetupPieceCoords(Coords pieceCoords)
        {
            PieceCoords = pieceCoords;
        }

        public override IEnumerator<Coords> GetEnumerator()
        {
            return new PieceMovementDirectionalEnumerator(Direction, PieceCoords);
        }
    }
}