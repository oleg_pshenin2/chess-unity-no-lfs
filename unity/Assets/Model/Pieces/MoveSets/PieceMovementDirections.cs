namespace TestChess.Model
{
    public enum PieceMovementDirections
    {
        Up,
        UpRight,
        Right,
        DownRight,
        Down,
        DownLeft,
        Left,
        UpLeft
    }
}