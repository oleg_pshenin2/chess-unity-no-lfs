using System.Collections;
using System.Collections.Generic;

namespace TestChess.Model
{
    public partial class PieceMovementOffsetJump
    {
        protected class PieceMovementDirectionalEnumerator : IEnumerator<Coords>
        {
            private readonly Coords _jumpOffset;
            private readonly Coords _pieceCoords;
            private readonly int _movesLimit;
            private int _currentIterationNumber;

            public Coords Current { get; private set; }
            object IEnumerator.Current => this.Current;

            public PieceMovementDirectionalEnumerator(Coords jumpOffset, Coords pieceCoords, int movesLimit = 0)
            {
                _jumpOffset = jumpOffset;
                _pieceCoords = pieceCoords;
                _movesLimit = movesLimit;

                Current = pieceCoords;
            }

            public bool MoveNext()
            {
                if (IsLimited())
                {
                    _currentIterationNumber++;
                    if (_currentIterationNumber > _movesLimit)
                        return false;
                }

                Current += _jumpOffset;

                return true;
            }

            public void Reset()
            {
                Current = _pieceCoords;
                _currentIterationNumber = 0;
            }

            public void Dispose() { }

            private bool IsLimited()
            {
                return _movesLimit > 0;
            }
        }
    }
}