namespace TestChess.Model
{
    public sealed class CastlingMove : Move
    {
        public CastlingMove(int moveToIndex, int rookFromIndex, int rookToIndex) : base(moveToIndex)
        {
            SpecialIndexes = new int[2] { rookFromIndex, rookToIndex };
        }

        public override MoveType GetMoveType()
        {
            return MoveType.Castling;
        }
    }
}