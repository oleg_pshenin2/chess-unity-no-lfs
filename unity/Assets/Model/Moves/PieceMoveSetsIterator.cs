using System;
using System.Collections.Generic;

namespace TestChess.Model
{
    public class PieceMoveSetsIterator
    {
        private readonly PieceMovement[] _sharedCastlingSet = new PieceMovement[2]
        {
            new PieceMovementDirectional (PieceMovementDirections.Right),
            new PieceMovementDirectional (PieceMovementDirections.Left),
        };

        private readonly IBoard _board;

        public PieceMoveSetsIterator(IBoard board)
        {
            _board = board;
        }

        /// <summary>
        /// IMPORTATNT: Ignores check status
        /// </summary>
        public List<Move> GetAvailableMovesForPieceFrom(int fromIndex)
        {
            List<Move> availableMoves = new List<Move>();
            var piece = _board.GetPiece(fromIndex);

            if (piece == null)
                return availableMoves;

            var isAttackSetTheSameAsMovement = piece.IsAttackSetTheSameAsMovement();

            IterateThroughMoveSetOfPiece(availableMoves, piece, fromIndex, isAttackSetTheSameAsMovement);

            if (!isAttackSetTheSameAsMovement)
                IterateThroughAttackSetOfPiece(availableMoves, piece, fromIndex);

            IterateThroughCastlingSetOfPiece(availableMoves, piece, fromIndex);

            return availableMoves;
        }

        private void IterateThroughMoveSetOfPiece(List<Move> availableMoves, Piece piece, int fromIndex, bool includeCapturing)
        {
            var pieceCoords = BoardUtils.GetCoordsByIndex(fromIndex);

            foreach (var movementIterator in piece.GetPieceMovementSet())
            {
                movementIterator.SetupPieceCoords(pieceCoords);

                foreach (var moveCoords in movementIterator)
                {
                    if (BoardUtils.IsInsideBoard(moveCoords))
                    {
                        var index = BoardUtils.GetIndexByCoords(moveCoords);
                        var pieceAtCoords = _board.GetPiece(moveCoords);

                        if (pieceAtCoords == null)
                        {
                            availableMoves.AddMoveIfNotContains(new RegularMove(index));
                        }
                        else
                        {
                            if (includeCapturing && piece.Color != pieceAtCoords.Color)
                                availableMoves.AddMoveIfNotContains(new RegularMove(index));

                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        private void IterateThroughAttackSetOfPiece(List<Move> availableMoves, Piece piece, int fromIndex)
        {
            var pieceCoords = BoardUtils.GetCoordsByIndex(fromIndex);

            foreach (var movementIterator in piece.GetPieceAttackSet())
            {
                movementIterator.SetupPieceCoords(pieceCoords);

                foreach (var moveCoords in movementIterator)
                {
                    if (BoardUtils.IsInsideBoard(moveCoords))
                    {
                        var index = BoardUtils.GetIndexByCoords(moveCoords);
                        var pieceAtCoords = _board.GetPiece(moveCoords);

                        if (pieceAtCoords != null)
                        {
                            if (piece.Color != pieceAtCoords.Color)
                                availableMoves.AddMoveIfNotContains(new RegularMove(index));

                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        private void IterateThroughCastlingSetOfPiece(List<Move> availableMoves, Piece piece, int fromIndex)
        {
            var pieceCoords = BoardUtils.GetCoordsByIndex(fromIndex);

            if (piece.IsMoved() || !piece.CanBeCastled())
                return;

            foreach (var movementIterator in _sharedCastlingSet)
            {
                movementIterator.SetupPieceCoords(pieceCoords);

                foreach (var moveCoords in movementIterator)
                {
                    if (BoardUtils.IsInsideBoard(moveCoords))
                    {
                        var moveToIndex = BoardUtils.GetIndexByCoords(moveCoords);
                        var pieceAtCoords = _board.GetPiece(moveCoords);

                        if (pieceAtCoords != null)
                        {
                            if (piece.Color == pieceAtCoords.Color && !pieceAtCoords.IsMoved() && pieceAtCoords.GetPieceType() == PieceType.Rook)
                            {
                                var endIndexOfCastlingMove = fromIndex + 2 * Math.Sign(moveToIndex - fromIndex);
                                var endIndexOfCastlingRookMove = fromIndex + 1 * Math.Sign(moveToIndex - fromIndex);

                                if (BoardUtils.IsInsideBoard(endIndexOfCastlingMove))
                                    availableMoves.AddMoveIfNotContains(new CastlingMove(endIndexOfCastlingMove, moveToIndex, endIndexOfCastlingRookMove));
                            }

                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
}