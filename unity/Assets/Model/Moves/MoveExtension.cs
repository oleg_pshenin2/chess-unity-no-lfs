using System.Collections.Generic;
using System.Linq;

namespace TestChess.Model
{
    public static class MoveExtension
    {
        public static void AddMoveIfNotContains(this List<Move> list, Move move)
        {
            if (!list.ContainsMove(move))
                list.Add(move);
        }

        public static bool ContainsMove(this List<Move> list, Move move)
        {
            return list.Any(x => x.MoveToIndex == move.MoveToIndex && x.GetMoveType() == move.GetMoveType());
        }

        public static bool ContainsMove(this List<Move> list, int toIndex)
        {
            return list.Any(x => x.MoveToIndex == toIndex);
        }
    }
}