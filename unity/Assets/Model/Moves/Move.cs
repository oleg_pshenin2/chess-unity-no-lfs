using System.Collections.Generic;

namespace TestChess.Model
{
    public abstract class Move
    {
        public abstract MoveType GetMoveType();

        public override bool Equals(object obj)
        {
            return obj is Move move &&
                   MoveToIndex == move.MoveToIndex &&
                   EqualityComparer<int[]>.Default.Equals(SpecialIndexes, move.SpecialIndexes);
        }

        public override int GetHashCode()
        {
            var hashCode = -1617331773;
            hashCode = hashCode * -1521134295 + MoveToIndex.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<int[]>.Default.GetHashCode(SpecialIndexes);
            return hashCode;
        }

        public readonly int MoveToIndex;
        public int[] SpecialIndexes;

        protected Move(int moveToIndex)
        {
            MoveToIndex = moveToIndex;
        }

    }
}