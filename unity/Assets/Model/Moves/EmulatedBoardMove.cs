namespace TestChess.Model
{
    public class EmulatedBoardMove
    {
        private readonly Square[] _squares;
        private Square _fromSquare;
        private Square _toSquare;
        private Piece _capturedPiece;

        public EmulatedBoardMove(Square[] squares)
        {
            _squares = squares;
        }

        public void Move(int fromIndex, int toIndex)
        {
            _fromSquare = _squares[fromIndex];
            _toSquare = _squares[toIndex];

            _capturedPiece = _toSquare.Piece;

            _toSquare.Piece = _fromSquare.Piece;
            _fromSquare.Piece = null;
        }

        public void Rollback()
        {
            _fromSquare.Piece = _toSquare.Piece;
            _toSquare.Piece = _capturedPiece;
        }
    }
}