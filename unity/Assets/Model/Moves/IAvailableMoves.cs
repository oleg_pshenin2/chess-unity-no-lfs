using System.Collections.Generic;

namespace TestChess.Model
{
    public interface IAvailableMoves
    {
        void SetCurrentPlayer(ChessColor color);
        void GetAvailableMovesSplittedByCapturing(int fromIndex, out List<int> availableMoves, out List<int> captureMoves);
        bool CanPieceMoveFromTo(int fromIndex, int toIndex);
        bool HasPlayerMoves(ChessColor player);
        bool IsPlayerKingUnderAttack(ChessColor player);
        void MakeAMove(int fromIndex, int toIndex);
    }
}