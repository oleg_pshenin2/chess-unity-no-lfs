using System;
using System.Collections.Generic;

namespace TestChess.Model
{
    public interface IBoard
    {
        event Action<int, int> PieceMovedFromTo;
        event Action<int> PieceCreatedAt;

        void SetupDefaultLayout();
        void SetupLayout(Dictionary<int, PieceType> whiteLayout, Dictionary<int, PieceType> blackLayout);
        Piece GetPiece(int index);
        Piece GetPiece(Coords coords);
        Square GetSquare(int index);
        Square[] GetSquares();
        void MovePiece(int fromIndex, int toIndex);
        List<Square> GetPiecesIndexesByPlayer(ChessColor player);
        void SetNewPieceAt(int index, PieceType pieceType, ChessColor color);
    }
}