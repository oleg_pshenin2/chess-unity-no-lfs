using System;

namespace TestChess.Model
{
    public static class BoardUtils
    {
        public static bool IsInsideBoard(Coords coords)
        {
            if (coords.X < 0 || coords.Y < 0 || coords.X >= Board.Size || coords.Y >= Board.Size)
                return false;
            else
                return true;
        }

        public static bool IsInsideBoard(int index)
        {
            if (index < 0 || index >= Board.Size * Board.Size)
                return false;
            else
                return true;
        }

        public static int GetIndexByCoords(int x, int y)
        {
            return y * Board.Size + x;
        }

        public static void GetCoordsByIndex(int index, out int x, out int y)
        {
            x = index % Board.Size;
            y = index / Board.Size;
        }

        public static int GetIndexByCoords(Coords coords)
        {
            return coords.Y * Board.Size + coords.X;
        }

        public static Coords GetCoordsByIndex(int index)
        {
            return new Coords(index % Board.Size, index / Board.Size);
        }

        public static ChessColor GetColorByCoords(int x, int y)
        {
            if ((x % 2) != (y % 2))
            {
                return ChessColor.White;
            }
            else
            {
                return ChessColor.Black;
            }
        }

        public static ChessColor GetOppositeColor(this ChessColor color)
        {
            return color == ChessColor.White ? ChessColor.Black : ChessColor.White;
        }

        public static void IterateThroughBoard(Action<int, int> _coordAction)
        {
            for (int y = 0; y < Board.Size; y++)
            {
                for (int x = 0; x < Board.Size; x++)
                {
                    var coordX = x;
                    var coordY = y;

                    _coordAction?.Invoke(coordX, coordY);
                }
            }
        }
    }
}