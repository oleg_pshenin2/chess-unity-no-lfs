﻿namespace TestChess.Model
{
    public class Square
    {
        private readonly int _index;

        public Piece Piece { get; set; }
        public int Index { get => _index; }

        public Square(int index)
        {
            _index = index;
        }
    }
}