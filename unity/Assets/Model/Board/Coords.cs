using System;

namespace TestChess.Model
{
    public struct Coords
    {
        public int X;
        public int Y;

        public Coords(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Coords operator +(Coords coords1, Coords coords2)
        {
            return new Coords(coords1.X + coords2.X, coords1.Y + coords2.Y);
        }

        public static Coords operator -(Coords coords1, Coords coords2)
        {
            return new Coords(coords1.X - coords2.X, coords1.Y - coords2.Y);
        }

        public static bool operator ==(Coords coords1, Coords coords2)
        {
            return (coords1.X == coords2.X && coords1.Y == coords2.Y);
        }

        public static bool operator !=(Coords coords1, Coords coords2)
        {
            return (coords1.X != coords2.X || coords1.Y != coords2.Y);
        }

        public override bool Equals(object obj)
        {
            return obj is Coords coords &&
                   X == coords.X &&
                   Y == coords.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }
    }
}